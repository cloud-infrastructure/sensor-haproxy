#! /usr/bin/env python

# imports
import socket
import time
import csv
import hashlib

# the sensor API interface
from sensorAPI import *

# register package name and version
registerVersion("lemon-sensor-haproxy", "1.1.0-2")

# register the sensor metric classes
registerMetric("haproxy.stats", "Reports if the haproxy cluster is partitioned", "sensor-haproxy.Statistics")
registerMetric("haproxy.status", "Reports the status of the frontends and backends in haproxy", "sensor-haproxy.Status")

class HaProxyMetric(Metric):
    '''
    Returns haproxy stats by connecting to the local unix socket

    Arguments accepted:
        socket : default /var/lib/haproxy/stats
    '''
    def postProcessSample(self, data):
        pass

    def sample(self):
        skt = '/var/lib/haproxy/stats'
        if 'socket' in self.parameters:
            skt = self.parameters['socket']

        try:
            # Connect to the socket
            client = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
            client.connect(skt)

            # Request the stats
            client.sendall("show stat\n")

            output = ""
            buf = ""

            # Reads the results
            buf = client.recv(4096)
            while buf:
                output += buf
                buf = client.recv(4096)
            client.close()
        except:
            self.log(SENSOR_LOG_ERROR, "Unable to retrieve haproxy statistics")
            return -1

        # Builds an list of dictionaries where every item contains the
        # stats for a single backend, frontend or server
        output = output.lstrip('# ').strip()
        output = [ l.strip(',') for l in output.splitlines() ]
        csvreader = csv.DictReader(output)

        # We keep only the frontend and backend. For the moment we avoid collecting data for the single members
        data = {}
        for service in csvreader:
            if service['svname'] in ["BACKEND","FRONTEND"]:
                srvhash = hashlib.md5(service["pxname"] + service["svname"]).hexdigest()
                data[srvhash] = {}
                data[srvhash]["pxname"] = service["pxname"]
                data[srvhash]["svname"] = service["svname"]
                for field in [ 'bin','bout', 'status',
                               'hrsp_1xx','hrsp_2xx','hrsp_2xx','hrsp_3xx','hrsp_4xx','hrsp_5xx',
                               'qtime','ctime','rtime','ttime',
                               'qcur','scur']:
                    if not service[field]:
                        data[srvhash][field] = 0
                    else:
                        try:
                            data[srvhash][field] = int(service[field])
                        except ValueError:
                            data[srvhash][field] = service[field]

        return self.postProcessSample(data);


class Statistics(HaProxyMetric):
    '''
    Returns haproxy stats

    Arguments accepted:
        socket : default /var/lib/haproxy/stats
    '''

    def setUp(self):
        ''' Setup method '''
        self.old_timestamp = None
        self.old_data = None

    def postProcessSample(self, data):
         
        timestamp = time.time()

        if self.old_timestamp:

            time_delta = float(timestamp - self.old_timestamp)

            # We create a sample only for the haproxy backends and frontends present in the current and past stat
            for srvhash in set(data.keys()) & set(self.old_data.keys()):
                bytes_in  = round((data[srvhash]['bin']      - self.old_data[srvhash]['bin'])      / time_delta ,2)
                bytes_out = round((data[srvhash]['bout']     - self.old_data[srvhash]['bout'])     / time_delta ,2)
                hrsp_1xx  = round((data[srvhash]['hrsp_1xx'] - self.old_data[srvhash]['hrsp_1xx']) / time_delta ,2)
                hrsp_2xx  = round((data[srvhash]['hrsp_2xx'] - self.old_data[srvhash]['hrsp_2xx']) / time_delta ,2)
                hrsp_3xx  = round((data[srvhash]['hrsp_3xx'] - self.old_data[srvhash]['hrsp_3xx']) / time_delta ,2)
                hrsp_4xx  = round((data[srvhash]['hrsp_4xx'] - self.old_data[srvhash]['hrsp_4xx']) / time_delta ,2)
                hrsp_5xx  = round((data[srvhash]['hrsp_5xx'] - self.old_data[srvhash]['hrsp_5xx']) / time_delta ,2)
                qtime     = data[srvhash]['qtime']
                ctime     = data[srvhash]['ctime']
                rtime     = data[srvhash]['rtime']
                ttime     = data[srvhash]['ttime']
                queue     = data[srvhash]['qcur']
                sessions  = data[srvhash]['scur']

                value = "%s %s %d %d %d %d %d %d %d %d %d %d %d %d %d" % \
                    (data[srvhash]["pxname"], data[srvhash]["svname"], \
                     bytes_in, bytes_out, \
                     hrsp_1xx, hrsp_2xx, hrsp_3xx, hrsp_4xx, hrsp_5xx, \
                     qtime, ctime, rtime, ttime,
                     queue, sessions)

                self.storeSample01(value)

        self.old_timestamp = timestamp
        self.old_data = data

        return 0


class Status(HaProxyMetric):
    '''
    Returns haproxy status

    Arguments accepted:
        socket : default /var/lib/haproxy/stats
    '''

    def postProcessSample(self, data):
        for srvhash in set(data.keys()):
            # Report only backend and frontend status
            if data[srvhash]['svname'] in ["BACKEND","FRONTEND"]:
                value = "%s %s %s" % (data[srvhash]["pxname"], data[srvhash]["svname"], data[srvhash]['status'])
            self.storeSample01(value)
        return 0
