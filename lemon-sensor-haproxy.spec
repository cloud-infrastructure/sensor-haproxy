# Specfile
Summary:     Lemon sensor haproxy
Name:        lemon-sensor-haproxy
Version:     1.1.0
Release:     2
License:     GPL
Group:       System Environment/Base
BuildRoot:   %{_tmppath}/%{name}-root
BuildArch:   noarch
Source:      %{name}-%{version}.tar.gz

%define _missing_doc_files_terminate_build 0
%define _unpackaged_files_terminate_build 0

%description
This package contains the lemon sensor for monitoring haproxy

%prep
%setup -n %{name}-%{version}

%install
rm -rf $RPM_BUILD_ROOT
make install prefix=$RPM_BUILD_ROOT

%clean
rm -rf $RPM_BUILD_ROOT
rm -rf $RPM_BUILD_DIR/%{name}-%{version}

%files
%defattr(-,root,root)
%doc AUTHORS ChangeLog COPYING
%doc docs/*.html docs/tpl/*.tpl

%config /usr/libexec/sensors/*
